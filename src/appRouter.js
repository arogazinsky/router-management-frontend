import React, {Component} from "react";
import {BrowserRouter as Router, Route, Link, Redirect} from "react-router-dom";
import {RouterDetails, RouterList, CreateNewRouter} from "./components/router";
import {CreateScript, DetailScript, ScriptList} from "./components/script";
import {CreateImplementation, ImplementationDetails, ImplementationsList} from "./components/implementation";
import {PasswordUpdate, UserCreate, UserList, UserUpdate} from "./components/user";
import {Navbar} from "./components/navbar";
import {LoginComponent} from "./components/login";
import {isLoginned} from "./components/service";
import {SettingsComponent} from "./components/settings";
import './App.css';

class PrivateRoute extends Route{
    constructor(props, router){
        super(props, router);
        this.state.isLoggined = true;
        isLoginned().then(
            (value) =>{
                let newState = this.state;
                newState.isLoggined = value;
                this.setState(newState)
            })
    }

    render() {
        return this.state.isLoggined ?
            (super.render())
            : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: { from: this.props.location }
                    }}
                />
            )
    }
}

class HomePageComponent extends Component{
    render() {
        return (
        <div className="container">
            <h2>Admin panel</h2>
            <p>Select items in menu to configure </p>
        </div>
        )
    }
}

const AppRouter = () => (
    <Router>
        <div>
            <PrivateRoute path="/" exact component={HomePageComponent} />
            <Route path="/login" exact component={LoginComponent} />
            <PrivateRoute path="/settings" exact component={SettingsComponent} />

            <PrivateRoute path="/routers" exact component={RouterList} />
            <PrivateRoute path="/routers/:id(\d+)" exact component={RouterDetails} />
            <PrivateRoute path="/routers/add" exact component={CreateNewRouter} />

            <PrivateRoute path="/scripts" exact component={ScriptList} />
            <PrivateRoute path="/scripts/:id(\d+)" exact component={DetailScript} />
            <PrivateRoute path="/scripts/add" exact component={CreateScript} />

            <PrivateRoute path="/scripts/:scriptId(\d+)/implementations/" exact component={ImplementationsList} />
            <PrivateRoute path="/scripts/:scriptId(\d+)/implementations/add" exact component={CreateImplementation} />
            <PrivateRoute path="/scripts/:scriptId(\d+)/implementations/:id(\d+)" exact component={ImplementationDetails} />

            <PrivateRoute path="/users" exact component={UserList} />
            <PrivateRoute path="/users/:id(\d+)" exact component={UserUpdate} />
            <PrivateRoute path="/users/:id(\d+)/reset_password" exact component={PasswordUpdate} />
            <PrivateRoute path="/users/add" exact component={UserCreate} />

            {/*<Route path="/users/" component={Users} />*/}
        </div>
    </Router>
);

const MainComponent = () => (
    [
    <Navbar/>,
    <AppRouter/>
    ]
);

export default MainComponent;