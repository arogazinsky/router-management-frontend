class BaseService {
    host = "http://localhost:8000/";
    apiURL = "http://localhost:8000/api/";

    makeRequest(url, method, body=undefined){
        let token = localStorage.getItem("token");
        let params = {
            method: method,
            headers: {
                Authorization: "Token " + token,
                "Content-Type": "application/json"
            }
        };
        if(body)
            params.body = JSON.stringify(body);
        return fetch(url, params).catch((error)=>{
            console.error(error);
        })
    }

    makeJSONRequest(url, method, body=undefined, onSuccess, onError){
        let token = localStorage.getItem("token");
        let params = {
            method: method,
            headers: {
                Authorization: "Token " + token,
                "Content-Type": "application/json"
            }
        };
        if(body)
            params.body = JSON.stringify(body);
        return fetch(url, params)
            .then(response =>{
                if(response.ok)
                    return response.json().then((data)=>Promise.resolve(data));
                else
                    return response.json().then((data)=>Promise.reject(data))
            }, (error)=>{
                console.error(error);
            });
    }
}

export class FakeRouterService extends BaseService {
    routers = "routers/";

    getRouters() {
        // let fullURL = this.apiURL + this.routers;
        // return fetch(fullURL, {
        //     method: "GET",
        //     headers: {
        //         "authorization": "Bearer " + this.token
        //     }
        // }).then((response)=>response.json());
        return Promise.resolve([
            {
                "ssh_connection": {
                    "host_name": "192.168.0.105",
                    "port": 22,
                    "username": "admin",
                    "password": "admin"
                },
                "id": 3,
                "name": "demo"
            },
            {
                "ssh_connection": {
                    "host_name": "10.8.66.198",
                    "port": 22,
                    "username": "user",
                    "password": "user"
                },
                "id": 4,
                "name": "demo2"
            },
            {
                "ssh_connection": {
                    "host_name": "10.8.66.198",
                    "port": 22,
                    "username": "test",
                    "password": "test"
                },
                "id": 5,
                "name": "test"
            }
        ])
    }

    getRouterDetails(routerId) {
        return Promise.resolve(
            {
                "ssh_connection": {
                    "host_name": "10.8.66.198",
                    "port": 22,
                    "username": "test",
                    "password": "test"
                },
                "id": routerId,
                "name": "test"
            }
        );
    }

    createRouter(data){
        return Promise.resolve({"id": 5})
    }

    updateRouter(routerId, data) {
        return Promise.resolve("OK")
    }

    deleteRouter(routerId){
        return Promise.resolve("OK")
    }

    getUsers(){
        let fullURL = this.apiURL + this.users;
        return fetch(fullURL, {
            method: "GET",
            headers: {
                "authorization": "Bearer " + this.token
            }
        }).then((response)=>response.json());
    }

    getScripts(){
        let fullURL = this.apiURL + this.scripts;
        return fetch(fullURL, {
            method: "GET",
            headers: {
                "authorization": "Bearer " + this.token
            }
        }).then((response)=>response.json());
    }
}

export class RouterService extends BaseService {
    routers = "routers/";

    getRouters() {
        let fullURL = this.apiURL + this.routers;
        console.log(fullURL);
        return this.makeJSONRequest(fullURL, 'get')
    }

    getRouterDetails(routerId) {
        let fullURL = this.apiURL + this.routers + routerId + "/";
        return this.makeJSONRequest(fullURL, 'get')
    }

    createRouter(data){
        let fullURL = this.apiURL + this.routers;
        return this.makeJSONRequest(fullURL, 'post', data)
    }

    updateRouter(routerId, data) {
        let fullURL = this.apiURL + this.routers + routerId + "/";
        console.log(data);
        return this.makeJSONRequest(fullURL, 'put', data)
    }

    deleteRouter(routerId){
        const fullURL = this.apiURL + this.routers + routerId + "/";
        return this.makeRequest(fullURL, 'delete')
    }

}

export class FakeImplementationsService extends BaseService {
    getScriptImplementations(scriptId){
        return Promise.resolve([
            {
                "commands": [
                    "ip address print"
                ],
                "id": 4,
                "router": "demo"
            },
            {
                "commands": [
                    "ip address print"
                ],
                "id": 7,
                "router": "demo2"
            }
        ])
    }

    getScriptImplementationDetails(scriptId, implementationId){
        return Promise.resolve(
            {
                "commands": [
                    "ip address print"
                ],
                "id": 4,
                "router": "demo"
            }
        )
    }

    addScriptImplementation(scriptId, data){
        data.id = 1;
        return Promise.resolve(data);
    }

    deleteScriptImplementation(scriptId, implementationId){
        return Promise.resolve("OK");
    }

    updateScriptImplementation(scriptId, implementationId, data){
        data.id = implementationId;
        return Promise.resolve(data);
    }
}

export class ImplementationsService extends BaseService {
    getScriptImplementations(scriptId){
        const fullURL = this.apiURL + "scripts/" + scriptId + "/implementations/";
        return this.makeJSONRequest(fullURL, "get");
    }

    getScriptImplementationDetails(scriptId, implementationId){
        const fullURL = this.apiURL + "scripts/" + scriptId + "/implementations/" + implementationId + "/";
        return this.makeJSONRequest(fullURL, "get");
    }

    addScriptImplementation(scriptId, data){
        const fullURL = this.apiURL + "scripts/" + scriptId + "/implementations/";
        return this.makeJSONRequest(fullURL, "post", data);
    }

    deleteScriptImplementation(scriptId, implementationId){
        const fullURL = this.apiURL + "scripts/" + scriptId + "/implementations/" + implementationId + "/";
        return this.makeRequest(fullURL, "delete");
    }

    updateScriptImplementation(scriptId, implementationId, data){
        const fullURL = this.apiURL + "scripts/" + scriptId + "/implementations/" + implementationId + "/";
        return this.makeJSONRequest(fullURL, "put", data);
    }
}

export class FakeScriptsService extends BaseService{
    addScript(data) {
        data.id = 1;
        return Promise.resolve(data)
    }

    getScripts() {
        return Promise.resolve([
            {
                "id": 1,
                "name": "RebootRouter",
                "description_text": "Перезапуск роутера",
                "allowed_for_users": [
                    "Jimhadar"
                ]
            },
            {
                "id": 2,
                "name": "DisableNAT",
                "description_text": "Выключение NAT",
                "allowed_for_users": [
                    "Jimhadar"
                ]
            },
            {
                "id": 3,
                "name": "SwitchPort",
                "description_text": "Включение/выключение портов",
                "allowed_for_users": [
                    "Jimhadar"
                ]
            }
        ])
    }

    getScriptDetails(scriptId){
        return Promise.resolve(
            {
                "id": 1,
                "name": "RebootRouter",
                "description_text": "Перезапуск роутера",
                "allowed_for_users": [
                    "user",
                    "manager"
                ]
            })
    }

    updateScript(scriptId, data) {
        data.id = scriptId;
        return Promise.resolve(data);
    }

    deleteScript() {
        return Promise.resolve("OK")
    }
}

export class ScriptsService extends BaseService{
    addScript(data) {
        const fullURL= this.apiURL + "scripts/";
        return this.makeJSONRequest(fullURL, "post", data);
    }

    getScripts() {
        const fullURL= this.apiURL + "scripts/";
        return this.makeJSONRequest(fullURL, "get");
    }

    getScriptDetails(scriptId){
        const fullURL= this.apiURL + "scripts/" + scriptId + "/";
        return this.makeJSONRequest(fullURL, "get");
    }

    updateScript(scriptId, data) {
        const fullURL= this.apiURL + "scripts/" + scriptId + "/";
        return this.makeJSONRequest(fullURL, "put", data);
    }

    deleteScript(scriptId) {
        const fullURL= this.apiURL + "scripts/" + scriptId + "/";
        return this.makeRequest(fullURL, "delete");
    }
}

export class UserService extends BaseService{
    getUsers(){
        const apiURL = this.apiURL + "users/";
        return this.makeJSONRequest(apiURL, "get");
    }

    getUserDetails(userId){
        const apiURL = this.apiURL + "users/" + userId + "/";
        return this.makeJSONRequest(apiURL, "get");
    }

    addUser(data){
        const apiURL = this.apiURL + "users/";
        return this.makeJSONRequest(apiURL, "post", data);
    }

    updateUser(userId, data){
        const apiURL = this.apiURL + "users/" + userId + "/";
        return this.makeJSONRequest(apiURL, "put", data);
    }

    deleteUser(userId){
        const apiURL = this.apiURL + "users/" + userId + "/";
        return this.makeRequest(apiURL, "delete");
    }

    resetPassword(userId, data){
        const apiURL = this.apiURL + "users/" + userId + "/reset_password/";
        return this.makeJSONRequest(apiURL, "post", data);
    }
}

export class LoginService extends BaseService{
    login(data) {
        const fullURL = this.host + "login/";
        const params = {
            method: "post",
            headers: {
                "Content-Type": "application/json"
            }
        };
        params.body = JSON.stringify(data);
        return fetch(fullURL, params).then((response)=>{
            if(response.ok)
                return response.json();
            else
                return response.json().then((errors) => Promise.reject(errors))
        },
        (error)=>{
            console.error(error);
        });
    }
}

export class SettingsService extends BaseService{
    getSettings(){
        const fullURL = this.apiURL + "settings";
        return this.makeJSONRequest(fullURL, "get");
    }

    postSettings(data){
        const fullURL = this.apiURL + "settings";
        return this.makeJSONRequest(fullURL, "post", data);
    }
}

export function isLoginned(){
    const token = localStorage.getItem("token");
    if(token === null)
        return Promise.resolve(false)
    const service = new SettingsService();
    return service.getSettings().then(
        (response) => true,
        (response) => false
    )
}
