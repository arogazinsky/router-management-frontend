import React, { Component } from 'react';
import {RouterService} from './service';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import {BaseForm, formatErrors} from "./base";

class RouterListItem extends Component{
  constructor(props) {
    super(props);
    this.state = this.props.router;
  }
  render() {
    return (
        <li className="list-group-item  d-flex justify-content-between">
          {this.state.name}
          <a href={"/routers/"+this.state.id}>Details</a>
        </li>
    );
  }
}

export class RouterList extends Component {
  constructor(props) {
    super(props);
    let service = new RouterService();
    this.state= {objects: []};
    service.getRouters().then((routers)=>{
      this.setState({objects: routers});
    });
  }

  getRoutersList() {
    let items = [];
    for(let router of this.state.objects){
        items.push(<RouterListItem router={router}/>)
    }
    return items;
  }

  render() {
    return (
    <div className="col-3">
      <h2>Routers</h2>
      <ul className="list-group">
        {this.getRoutersList()}
      </ul>
      <a href="/routers/add">
        <button className="btn btn-primary">Add new router</button>
      </a>
    </div>
    );
  }
}

class RouterForm extends BaseForm {
    onSubmit(values, actions) {
      if(values["ssh_connection"]["password"])
        super.onSubmit(values, actions);
      values["ssh_connection"]["password"] = "";
      return super.onSubmit(values, actions);
    }

  getFormFields(){
        return [
          <div className="form-group">
            <label>
              Router name
              <Field type="text" name="name" className="form-control"/>
            </label>
            <ErrorMessage name="name" component="div" className="alert alert-danger"/>
          </div>,
          <div className="form-group">
            <label>
              Host name
              <Field type="text" name="ssh_connection.host_name" className="form-control"/>
            </label>
            <ErrorMessage name="ssh_connection.host_name" component="div" className="alert alert-danger"/>
          </div>,
          <div className="form-group">
            <label>
              Username
              <Field type="text" name="ssh_connection.username" className="form-control"/>
            </label>
            <ErrorMessage name="ssh_connection.username" component="div" className="alert alert-danger"/>
          </div>,
          <div className="form-group">
            <label>
              Password
              <Field type="password" name="ssh_connection.password" className="form-control"/>
            </label>
            <ErrorMessage name="ssh_connection.password" component="div" className="alert alert-danger"/>
          </div>
        ]
    }
}

export class RouterDetails extends Component{
  constructor(props){
    super(props);
    this.state = {ssh_connection: {}};
    this.service = new RouterService();
    this.routerId = this.props.match.params.id;
    console.log(this.props);
    this.service.getRouterDetails(this.routerId).then((routerDetails)=>{
      this.setState(routerDetails);
    });
  }

  deleteRouter(){
    this.service.deleteRouter(this.routerId).then(()=>{this.props.history.push("/routers/")});
  }

  getAdditionalButtons(){
    let buttons = [];
    buttons.push(<div className="btn btn-danger" onClick={() => this.deleteRouter()}>Delete</div>);
    buttons.push(<div className="btn btn-secondary" onClick={() => {this.props.history.push("/routers/")}}>Routers List</div>);
    return buttons
  }

  onSubmit(values, actions){
    this.service.updateRouter(this.routerId, values).then(
        (data)=>{
          actions.setSubmitting(false);
          this.props.history.push("/routers/"+this.routerId);
        },
        (errors)=>{
          let errorMessages = formatErrors(errors);
          actions.setErrors(errorMessages);
          actions.setSubmitting(false);
        }
    )
  }

  render() {
    return (
        <div className="col-6">
          <h2>Router details</h2>
          <RouterForm object={this.state} onSubmit={(values, actions)=>{this.onSubmit(values, actions)}} additionalButtons={() => this.getAdditionalButtons()} submitText="Save Changes"/>
        </div>
    )
  }
}

export class CreateNewRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.service = new RouterService();
  }

  onSubmit(values, actions){
    this.service.createRouter(values).then(
        (data)=>{
          this.props.history.push("/routers/"+data.id);
          actions.setSubmitting(false);
        },
        (errors)=>{
          let errorMessages = formatErrors(errors);
          actions.setErrors(errorMessages);
          actions.setSubmitting(false);
        }
    )
  }

  getAdditionalButtons(){
    return <button onClick={() => {this.props.history.goBack()}} className="btn btn-secondary">Routers List</button>
  }

  render() {
    return (
        <div className="col-6">
          <h2>Add new router</h2>
          <RouterForm object={
            {
              name:"",
              ssh_connection:
                  {
                    host_name:"",
                    username:"",
                    password:""
                  }
            }
          } onSubmit={(values, actions)=>{this.onSubmit(values, actions)}} additionalButtons={() => this.getAdditionalButtons()} submitText="Create Router"/>
        </div>
    )
  }
}
