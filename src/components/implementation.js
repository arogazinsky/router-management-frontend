import React, {Component} from 'react';
import {BaseForm, formatErrors} from "./base";
import {ErrorMessage, Field} from "formik";
import {ImplementationsService, RouterService, ScriptsService} from "./service";

function convertFormValues(implementation) {
    let commands = implementation.commands;
    if("commands" in implementation && Array.isArray(implementation.commands))
        commands = commands.join("\n");
    return {
        router: implementation.router,
        commands: commands
    };
}

class ImplementationForm extends BaseForm {
    constructor(props) {
        super(props);
        const service = new RouterService();
        this.state = {
            script: {id: this.props.scriptId},
            availableValues:[]
        };
        service.getRouters().then((routers)=>{
            const names = routers.map((router)=>router.name);
            this.setState({availableValues: names});
        });
        const scriptService = new ScriptsService();
        scriptService.getScriptDetails(this.props.scriptId).then((data)=>{
            this.setState({
                script: data,
                availableValues: this.state.availableValues
            });
        });
    }

    onSubmit(values, actions) {
        if(values.router === "")
            delete values.router;
        if (typeof values.commands === 'string' || values.commands instanceof String)
            values.commands = values.commands.split("\n");
        super.onSubmit(values, actions);
    }

    formatInitialValues(values) {
        if(Array.isArray(values["commands"]))
            values["commands"] = values["commands"].join("\n");
        return values
    }

    getFormFields(values, errors, status, touched, isSubmitting, setFieldValue) {
        return [
            <div className="form-group">Script name: {this.state.script.name}</div>,
            <div className="form-group">
                <label>
                    Router
                    <Field component="select" name="router" className="form-control">
                        <option value="" label="" />
                        {this.state.availableValues.map(value => (
                            <option key={value} value={value} label={value}/>
                        ))}
                    </Field>
                    <ErrorMessage className="alert alert-danger" component="div" name="router"/>
                </label>
            </div>,
            <div className="form-group">
                <label>
                    Commands
                    <Field component="textarea" name="commands" className="form-control">
                    </Field>
                    <ErrorMessage className="alert alert-danger" component="div" name="commands"/>
                </label>
            </div>
        ]
    }
}

export class CreateImplementation extends Component{
    constructor(props){
        super(props);
        this.scriptId = this.props.match.params.scriptId;
        this.service = new ImplementationsService();
    }

    getAdditonalButtons(){
        return [
            <div className="btn btn-secondary">Implementations List</div>
        ]
    }

    onSubmit(values, actions){
        this.service.addScriptImplementation(this.scriptId, values).then((data)=>{
            this.props.history.push("/scripts/" + this.scriptId + "/implementations/" + data.id);
        },
        (errors)=>{
            let errorMessages = formatErrors(errors);
            if("commands" in errorMessages)
                errorMessages["commands"] = Array.prototype.join.call(errorMessages["commands"], " ");
            actions.setErrors(errorMessages);
            actions.setSubmitting(false);
        }
    )
    }

    render() {
        return (
            <div className="col-6">
                <h2>Add script implementation</h2>
                <ImplementationForm object={{router:"", commands:[]}} scriptId={this.props.match.params.scriptId} onSubmit={(values, actions)=>this.onSubmit(values, actions)} additionalButtons={()=>this.getAdditonalButtons()} submitText="Create Implementation"/>
            </div>
        )
    }
}

export class ImplementationDetails extends Component{
    constructor(props){
        super(props);
        this.scriptId = this.props.match.params.scriptId;
        const implementationId = this.props.match.params.id;
        this.state = {id: implementationId};
        this.implementationService = new ImplementationsService();
        this.implementationService.getScriptImplementationDetails(this.scriptId, implementationId).then((data)=>{
            this.setState(data);
        });
    }

    onSubmit(values, actions){
        this.implementationService.updateScriptImplementation(this.scriptId, this.state.id, values)
            .then((data)=>{
                this.setState(data);
                actions.setSubmitting(false);
            },
            (errors)=>{
                let errorMessages = formatErrors(errors);
                actions.setErrors(errorMessages);
                actions.setSubmitting(false);
            }
        )
    }

    deleteImplementation(){
        this.implementationService.deleteScriptImplementation(this.scriptId, this.state.id).then(()=>{this.props.history.push("/scripts/" + this.scriptId + "/implementations")});
    }

    getAdditionalButtons(){
        return [
            <div className="btn btn-secondary" onClick={()=>this.props.history.push("/scripts/" + this.scriptId + "/implementations")}>Implementations List</div>,
            <div className="btn btn-danger" onClick={()=>this.deleteImplementation()}>Delete implementation</div>
        ];
    }

    render() {
        return (
            <div className="col-6">
                <h2>Implementation details</h2>
                <ImplementationForm object={convertFormValues(this.state)} scriptId={this.props.match.params.scriptId} onSubmit={(values, actions)=>this.onSubmit(values, actions)} additionalButtons={()=>this.getAdditionalButtons()} submitText="Update Implementation"/>
            </div>
        )
    }
}

export class ImplementationsList extends Component {
    constructor(props){
        super(props);
        this.scriptId = this.props.match.params.scriptId;
        this.state = {
            scriptName: "",
            objects: [],
        };
        this.implementationService = new ImplementationsService();
        this.implementationService.getScriptImplementations(this.scriptId).then((data)=>{
            this.setState({
                objects: data,
                scriptName: this.state.scriptName
            });
        });
        this.scriptService = new ScriptsService();
        this.scriptService.getScriptDetails(this.scriptId).then((data)=>{
            this.setState({
                objects: this.state.objects,
                scriptName: data["name"]
            });
        });
    }

    render() {
        return (
            <div className="col-6">
                <h2>Script "{this.state.scriptName}" Implementations list</h2>
                <ul className="list-group">
                    {this.state.objects.map((implementation)=>
                        (<li className="list-group-item  d-flex justify-content-between">
                            <div>Implementation for router {implementation.router} </div>
                            <a href={"/scripts/" + this.scriptId + "/implementations/" + implementation.id}>Details</a>
                        </li>)
                    )}
                </ul>
                <a href={"/scripts/" + this.scriptId + "/implementations/add"}>
                    <button className="btn btn-primary">Add new implementation</button>
                </a>
            </div>
        )
    }
}
