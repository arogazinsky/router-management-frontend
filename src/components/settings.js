import React, {Component} from 'react'
import {SettingsService} from "./service";
import {ErrorMessage, Field, Formik} from "formik";
import {BaseForm, formatErrors} from "./base";


class SettingsForm extends BaseForm{
    getFormFields(values, errors, status, touched, isSubmitting, setFieldValue) {
        return [
            <div className="form-group">
                <label>
                    Telegram bot token
                    <Field type="text" name="TelegramBotToken" className="form-control"/>
                    <ErrorMessage component="div" name="TelegramBotToken" className="alert alert-danger"/>
                </label>
            </div>,
            <div className="form-group">
                <label>
                    Unknown user authorisation token
                    <Field type="text" name="AuthToken" className="form-control"/>
                    <ErrorMessage component="div" name="AuthToken" className="alert alert-danger"/>
                </label>
            </div>,
            <div className="form-group">
                <label>
                    Common telegram channel ID
                    <Field type="text" name="CommonChannelId" className="form-control"/>
                    <ErrorMessage component="div" name="CommonChannelId" className="alert alert-danger"/>
                </label>
            </div>,
            <div className="form-group">
                <label>
                    Bot State
                    <Field component="select" name="BotState" className="form-control">
                        <option value={null} label=""/>
                        <option value="stopped"  label="Stopped"/>
                        <option value="active" label="Active"/>
                    </Field>
                    {/*<Field type="checkbox" name="BotIsActive" className="form-control" checked={values.BotIsActive == "true"}/>*/}
                    <ErrorMessage component="div" name="BotState" className="alert alert-danger"/>
                </label>
            </div>
        ]
    }

}

export class SettingsComponent extends Component{
    constructor(props){
        super(props);
        this.service = new SettingsService();
        this.state = {};
        this.service.getSettings().then((data)=>{this.setState(data)});
    }

    onSubmit(values, actions) {
        this.service.postSettings(values).then(()=>{
            actions.setSubmitting(false);
        },
        (errors)=>{
            let errorMessages = formatErrors(errors);
            actions.setErrors(errorMessages);
            actions.setSubmitting(false);
        })
    }

    render() {
        return (
            <div className="col-6">
                <h2>Service settings</h2>
                <SettingsForm
                    object={this.state}
                    onSubmit={(values, actions)=>this.onSubmit(values, actions)}
                    additionalButtons={()=>[]}
                    submitText={"Update settings"}/>
            </div>
        );
    }
}
