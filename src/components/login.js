import React, {Component} from 'react'
import {LoginService} from "./service";
import {ErrorMessage, Field, Form, Formik} from "formik";
import {formatErrors} from "./base";

export class LoginComponent extends Component{
    constructor(props){
        super(props);
        this.service = new LoginService();
    }

    login(values, actions){
        this.service.login(values).then((response)=>{
            window.localStorage.setItem("token", response.token);
            this.props.history.push("/");
        },
        (errors)=>{
            let errorMessages = formatErrors(errors);
            actions.setErrors(errorMessages);
            actions.setSubmitting(false);
        })
    }

    render() {
        return (
            <div className="container col-4">
                <h2>Admin Login</h2>
                <p>Please enter your username and password</p>
                <Formik
                    onSubmit={(values, actions)=>this.login(values, actions)}
                    render={({values, errors, status, touched, isSubmitting, setFieldValue }) => (
                        <Form id="Login">
                            {
                                errors.non_field_errors ?
                                    (<div className="alert alert-danger">{errors.non_field_errors}</div>
                                    ): null
                            }
                            <div className="form-group">
                                <Field type="text" className="form-control" name="username" placeholder="Username"/>
                                <ErrorMessage className="alert alert-danger" component="div" name="username"/>
                            </div>
                            <div className="form-group">
                                <Field type="password" className="form-control" name="password" placeholder="Password"/>
                                <ErrorMessage className="alert alert-danger" component="div" name="password"/>
                            </div>
                            <button type="submit" className="btn btn-primary">Login</button>
                        </Form>
                    )}
                />
            </div>
        )
    };
}