import React, {Component} from 'react';
import {BaseForm, formatErrors} from "./base";
import {ErrorMessage, Field} from "formik";
import {UserService} from "./service";

class UserForm extends BaseForm {
    getFormFields(values, errors, status, touched, isSubmitting, setFieldValue) {
        return [
            <div className="form-group">
                <label>
                    Username
                    <Field type="text" name="username" className="form-control"/>
                    <ErrorMessage component="div" name="username" className="alert alert-danger"/>
                </label>
            </div>,
            <div className="form-group">
                <label>
                Is admin
                    <Field type="checkbox" name="is_staff" className="form-control" checked={values.is_staff == true} />
                    <ErrorMessage component="div" name="is_staff" className="alert alert-danger"/>
                </label>
            </div>
        ]
    }
}

class PasswordUpdateForm extends BaseForm {
    getFormFields(values, errors, status, touched, isSubmitting, setFieldValue) {
        return [
            <div className="form-group">
                <label>
                    Password
                    <Field type="password" name="new_password" className="form-control"/>
                    <ErrorMessage component="div" name="new_password" className="alert alert-danger"/>
                </label>
            </div>,
            <div className="form-group">
                <label>
                    Password confirm
                    <Field type="password" name="password_confirm" className="form-control"/>
                    <ErrorMessage component="div" name="password_confirm" className="alert alert-danger"/>
                </label>
            </div>
        ]
    }
}

export class PasswordUpdate extends Component{
    constructor(props){
        super(props);
        this.state = {id: this.props.match.params.id};
        this.service = new UserService();
    }

    onSubmit(values, actions){
        this.service.resetPassword(this.state.id, values).then(()=>{
            this.props.history.push("/users/" + this.state.id);
        }, (errors)=>{
            let errorMessages = formatErrors(errors);
            actions.setErrors(errorMessages);
            actions.setSubmitting(false);
        });
    }

    getAdditionalButtons(){
        return [
            <div className="btn btn-secondary" onClick={()=>{this.props.history.push("/users/" + this.state.id)}}>User details</div>
        ]
    }

    render() {
        return (
            <div className="col-6">
                <h2>Enter new user password</h2>
                <PasswordUpdateForm object={{"new_password":"", "password_confirm":""}} onSubmit={(values, actions)=>{this.onSubmit(values, actions)}} additionalButtons={()=>this.getAdditionalButtons()} submitText="Update password"/>
            </div>
        )
    }
}

export class UserCreate extends Component{
    constructor(props){
        super(props);
        this.service = new UserService();
    }

    onSubmit(values, actions) {
        this.service.addUser(values).then(
            (user)=>{this.props.history.push("/users/" + user.id)},
            (errors)=>{
                let errorMessages = formatErrors(errors);
                actions.setErrors(errorMessages);
                actions.setSubmitting(false);
            }
        );
    }

    getAdditionalButtons() {
        return [
            <div className="btn btn-secondary">Set password</div>,
            <div className="btn btn-secondary" onClick={()=>{this.props.history.push("/users/")}}>Users list</div>
        ]
    }

    render() {
        return (
            <div className="col-6">
                <h2>Add new telegram user</h2>
                <UserForm object={{"username":"", "is_staff":false}} onSubmit={(values, actions)=>{this.onSubmit(values, actions)}} additionalButtons={()=>this.getAdditionalButtons()} submitText="Add user"/>
            </div>
        )
    }
}

export class UserUpdate extends Component{
    constructor(props){
        super(props);
        this.service = new UserService();
        this.state = {id: this.props.match.params.id};
        this.service.getUserDetails(this.state.id).then((data)=>{
           this.setState(data);
        });
    }

    onSubmit(values, actions) {
        this.service.updateUser(this.state.id, values).then(
            (user)=>{actions.setSubmitting(false)},
            (errors)=>{
                let errorMessages = formatErrors(errors);
                actions.setErrors(errorMessages);
                actions.setSubmitting(false);
            });
    }

    deleteUser(){
        this.service.deleteUser(this.state.id).then(()=>{this.props.history.push("/users/")});
    }

    getAdditionalButtons() {
        return [
            <div className="btn btn-secondary" onClick={()=>{this.props.history.push("/users/" + this.state.id + "/reset_password")}}>Set password</div>,
            <div className="btn btn-secondary" onClick={()=>{this.props.history.push("/users/")}}>Users list</div>,
            <div className="btn btn-secondary" onClick={()=>{this.deleteUser()}}>Delete User</div>
        ]
    }

    render() {
        return (
            <div className="col-6">
                <h2>Update telegram user</h2>
                <UserForm object={this.state} onSubmit={(values, actions)=>{this.onSubmit(values, actions)}} additionalButtons={()=>this.getAdditionalButtons()} submitText="Update user"/>
            </div>
        )
    }
}

export class UserList extends Component {
    constructor(props) {
        super(props);
        this.state = {objects: []};
        this.service = new UserService();
        this.service.getUsers().then(
            (users)=>{
                this.setState({objects: users});
            }
        )
    }

    getUsersItems(){
        let items = [];
        for(let user of this.state.objects){
            items.push(
                <li className="list-group-item d-flex justify-content-between">
                    <div>{user.username}</div>
                    <a href={"/users/"+user.id}>Details</a>
                </li>
            );
        }
        return items;
    }

    render() {
        return (
            <div className="col-3">
                <h2>Users</h2>
                <ul className="list-group">
                {this.getUsersItems()}
                </ul>
                <button onClick={()=>this.props.history.push("/users/add")} className="btn btn-primary">Add user</button>
            </div>
        )
    }
}