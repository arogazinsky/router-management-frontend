import React, {Component} from 'react';

export class Navbar extends Component {
  render() {
    const urls = [
        {url:"login", text:"Login"},
        {url:"settings", text:"Settings"},
        {url:"routers", text:"Routers"},
        {url:"scripts", text:"Scripts"},
        {url:"users", text:"Users"}
    ];
    let current = window.location.pathname.split('/')[1];
    console.log(current);
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    {urls.map((item)=>(
                        <li className={"nav-item" + ((current === item.url)? " active": "")}>
                            <a className="nav-link" href={"/" + item.url}>{item.text}</a>
                        </li>
                    ))}
                </ul>
            </div>
        </nav>
    );
  }
};
