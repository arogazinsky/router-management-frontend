import { Component } from 'react';
import {ErrorMessage, Field, Form, Formik} from "formik";
import React from "react";
import {ScriptsService, UserService} from "./service";
import {BaseForm, formatErrors} from "./base";

class ScriptForm extends BaseForm{
    constructor(props) {
        super(props);
        const userService = new UserService();
        this.state = {availableUsers :[]};
        userService.getUsers().then((users)=>{
            this.setState({
                availableUsers : users.map((user)=>user.username)
            });
        })
    }

    onSubmit(values, actions){
        if(!("allowed_for_users" in values))
            values.allowed_for_users = [];
        this.props.onSubmit(values, actions);
    }

    getFormFields(values, errors, status, touched, isSubmitting, setFieldValue){
        return [
        <div className="form-group">
            <label>
                Script name
                <Field type="text" name="name" className="form-control"/>
            </label>
            <ErrorMessage name="name" component="div" className="alert alert-danger"/>
        </div>,
        <div className="form-group">
            <label>
            Description
            <Field type="text" name="description_text" className="form-control"/>
            </label>
            <ErrorMessage name="description_text" component="div" className="alert alert-danger"/>
        </div>,
        <div className="form-group">
            <label>
                Allowed for users

                <Field
                    component="select"
                    name="allowed_for_users"
                    className="form-control"
                    // You need to set the new field value
                    onChange={evt =>
                        setFieldValue(
                            "allowed_for_users",
                            [].slice
                                .call(evt.target.selectedOptions)
                                .map(option => option.value)
                        )
                    }
                    multiple={true}
                >
                    {this.state.availableUsers.map(user => (
                        <option key={user} value={user}>
                            {user}
                        </option>
                    ))}
                </Field>
                <ErrorMessage component="div" name="allowed_for_users" className="alert alert-danger"/>
            </label>
        </div>
        ]

    }
}

export class CreateScript extends Component{
    constructor(props){
        super(props);
        this.scriptService = new ScriptsService();
    }

    onSubmit(data, actions){
        this.scriptService.addScript(data)
            .then((data) => {
                actions.setSubmitting(false);
                this.props.history.push("/scripts/"+data.id);
            },
            (errors)=>{
                let errorMessages = formatErrors(errors);
                actions.setErrors(errorMessages);
                actions.setSubmitting(false);
            })
    }

    getAdditionalButtons(){
        return (
            <button onClick={()=>{this.props.history.goBack()}} className="btn btn-secondary"> Back </button>
        )
    }

    render() {
        return (
            <div className="col-6">
                <h2>Add new script</h2>
                <ScriptForm object={{name:"", description_text:"", allowed_for_users:[]}} onSubmit={(data, actions)=>this.onSubmit(data, actions)} additionalButtons={()=>{this.getAdditionalButtons()}} submitText="Create Script"/>
            </div>
        )
    }
}

export class DetailScript extends Component{
    constructor(props){
        super(props);
        this.service = new ScriptsService();
        this.state = {};
        console.log(this.props);
        this.scriptId = this.props.match.params.id;
        this.service.getScriptDetails(this.scriptId).then((data) => {this.setState(data)});
    }

    onSubmit(data, actions){
        this.service.updateScript(this.scriptId, data).then(()=>{
            actions.setSubmitting(false);
        },
        (errors)=>{
            let errorMessages = formatErrors(errors);
            actions.setErrors(errorMessages);
            actions.setSubmitting(false);
        });
    }

    onDelete(){
        this.service.deleteScript(this.scriptId).then(()=>{this.props.history.push("/scripts/")});
    }

    getAdditionalButtons(){
        return [
        <div className="btn btn-secondary" onClick={()=>{this.props.history.push("/scripts/" + this.scriptId + "/implementations/")}}>Script Implementations</div>,
        <div className="btn btn-danger" onClick={()=>{this.onDelete()}}>Delete script</div>,
        <div className="btn btn-secondary" onClick={()=>{this.props.history.push("/scripts/")}}>Scripts list</div>
        ]
    }

    render() {
        return (
            <div className="col-6">
                <h2>Script details</h2>
                <ScriptForm object={this.state} onSubmit={(data, actions)=>this.onSubmit(data, actions)} additionalButtons={()=>this.getAdditionalButtons()} submitText="Update Script"/>
            </div>
        )
    }
}

class ScriptListItem extends Component{
    constructor(props){
        super(props);
        this.state = props.script;
    }

    render() {
        return (
            <li className="list-group-item  d-flex justify-content-between">
                <div>{this.state.name}</div>
                <a href={"/scripts/"+this.state.id}>Details</a>
            </li>
        )
    }
}

export class ScriptList extends Component{
    constructor(props){
        super(props);
        this.scriptService = new ScriptsService();
        this.state= {objects: []};
        this.scriptService.getScripts().then((scripts)=>{
            this.setState({objects: scripts});
        });
    }

    getScriptListItems(){
        let items = [];
        for(let script of this.state.objects)
            items.push(<ScriptListItem script={script}/>);
        return items;
    }

    render() {
        return (
            <div className="col-3">
                <h2>Scripts</h2>
                <ul className="list-group">
                    {this.getScriptListItems()}
                </ul>
                <a href="/scripts/add">
                    <button className="btn btn-primary">Add new script</button>
                </a>
            </div>
        )
    }
}