import React, { Component } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';

export class BaseForm extends Component{
    getFormFields(values, errors, status, touched, isSubmitting, setFieldValue){
        return undefined
    }

    onSubmit(values, actions){
        this.props.onSubmit(values, actions);
    }

    formatInitialValues(values){
        return values
    }

    render() {
        return (
            <Formik
                enableReinitialize
                initialValues={this.formatInitialValues(this.props.object)}
                onSubmit={(values, actions)=>this.onSubmit(values, actions)}
                render={({values, errors, status, touched, isSubmitting, setFieldValue }) => (
                    <Form>
                        {
                            errors.non_field_errors ?
                            (<div className="alert alert-danger">{errors.non_field_errors}</div>
                            ): null
                        }
                        {this.getFormFields(values, errors, status, touched, isSubmitting, setFieldValue)}
                        <div className="button-container">
                            <button className="btn btn-primary" type="submit" disabled={isSubmitting}>
                                {this.props.submitText}
                            </button>
                            {this.props.additionalButtons()}
                        </div>
                    </Form>
                )}
            />
        )
    }

}

export function formatErrors(errors){
    let result = {};
    for(let key in errors){
        if(Array.isArray(errors[key]))
            result[key] = errors[key].join('\n');
        else
            if(errors[key] === Object(errors[key]))
                result[key] = formatErrors(errors[key]);
            else
                result[key] = errors[key]
    }
    return result;
}
